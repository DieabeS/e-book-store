import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';

import 'contents/body.dart';

class SignUp extends StatelessWidget {
  static String routeName = '/sign_up';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Sign Up',
          style: TextStyle(
            fontSize: getProportionateScreenWidth(18),
          ),
        ),
        centerTitle: true,
      ),
      body: Body(),
    );
  }
}
