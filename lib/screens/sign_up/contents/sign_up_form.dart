import 'package:e_book_store/components/custom_suffix_icon.dart';
import 'package:e_book_store/shared/constants.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: Column(
          children: [
            buildEmailForm(),
            SizedBox(
              height: getProportionateScreenWidth(30),
            ),
            buildUserNameForm(),
            SizedBox(
              height: getProportionateScreenWidth(30),
            ),
               buildPasswordForm(),
            SizedBox(
              height: getProportionateScreenWidth(30),
            ),
            buildRePasswordForm(),
            
          ],
        ),
      ),
    );
  }

  TextFormField buildRePasswordForm() {
    return TextFormField(
            decoration: InputDecoration(
                suffixIcon: CustomSufffixIcon(
                  icon: Icons.lock_outline,
                ),
                labelText: 'Repeat Password',
                labelStyle: formTextStyle(),
                alignLabelWithHint: true),
            obscureText: true,
            style: formTextStyle(),
          );
  }

  TextFormField buildPasswordForm() {
    return TextFormField(
      decoration: InputDecoration(
          suffixIcon: CustomSufffixIcon(
            icon: Icons.lock_outline,
          ),
          labelText: 'Password',
          labelStyle: formTextStyle(),
          alignLabelWithHint: true),
      obscureText: true,
      style: formTextStyle(),
    );
  }

  TextFormField buildEmailForm() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          suffixIcon: CustomSufffixIcon(
            icon: Icons.mail_outline,
          ),
          labelText: 'Email',
          labelStyle: formTextStyle(),
          alignLabelWithHint: true),
      style: formTextStyle(),
    );
  }
    TextFormField buildUserNameForm() {
    return TextFormField(
      decoration: InputDecoration(
          suffixIcon: CustomSufffixIcon(
            icon: Icons.person
          ),
          labelText: 'User Name',
          labelStyle: formTextStyle(),
          alignLabelWithHint: true),
      style: formTextStyle(),
    );
  }
}
