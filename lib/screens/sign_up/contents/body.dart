import 'package:e_book_store/components/default_button.dart';
import 'package:e_book_store/screens/complete_profile/complete_profile.dart';
import 'package:e_book_store/screens/sign_up/contents/sign_up_form.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
      
              Text(
                "Create an Account",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: getProportionateScreenWidth(28),
                  fontWeight: FontWeight.bold,
                ),
              ),
                 SizedBox(
                height: getProportionateScreenWidth(20),
              ),
              TweenAnimationBuilder(
                  tween: Tween<double>(begin: 0, end: 1),
                  duration: Duration(milliseconds: 4000),
                  curve: Curves.easeIn,
                  builder: (context, _, Widget child) {
                    return Opacity(
                      opacity: _,
                      child: SvgPicture.asset(
                        'assets/images/signUp.svg',
                        height: getProportionateScreenWidth(200),
                        width: getProportionateScreenWidth(200),
                      ),
                    );
                  }),
              SizedBox(
                height: getProportionateScreenWidth(30),
                
              ),
              SignUpForm(),
                 SizedBox(
                height: getProportionateScreenWidth(50),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: DefaultButton(text: 'Sign Up', press: () {Navigator.pushNamed(context, CompleteProfile.routeName);}),
              ),
    SizedBox(
                height: getProportionateScreenWidth(50),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
