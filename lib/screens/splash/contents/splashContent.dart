import 'package:e_book_store/shared/constants.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashContent extends StatelessWidget {
  const SplashContent({
    Key key,
    @required this.text,
    @required this.image,
  }) : super(key: key);
  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: getProportionateScreenWidth(20),
        ),
        Text(
          "DieabeS Library",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(36),
            color: kPrimaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(
          height: getProportionateScreenWidth(50),
        ),
        TweenAnimationBuilder(
            tween: Tween<double>(begin: 0, end: 1),
            curve: Curves.easeIn,
            duration: Duration(milliseconds: 500),
            builder: (context, _, Widget child) {
              return Column(
                children: [
                  SvgPicture.asset(
                    image,
                    height: _ * getProportionateScreenWidth(240),
                    width: _ * getProportionateScreenWidth(240),
                  ),
                ],
              );
            }),
        Spacer(),
        TweenAnimationBuilder(
            tween: Tween<double>(begin: 0, end: 1),
            curve: Curves.easeIn,
            duration: Duration(milliseconds: 1000),
            builder: (context, _, Widget child) {
              return Opacity(
                opacity: _,
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: getProportionateScreenWidth(18)),
                ),
              );
            }),
      ],
    );
  }
}
