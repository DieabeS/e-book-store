import 'package:e_book_store/components/default_button.dart';
import 'package:e_book_store/screens/sign_in/sign_in.dart';
import 'package:e_book_store/screens/splash/contents/splashContent.dart';
import 'package:e_book_store/shared/constants.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int currentPage = 0;
  List<Map<String, String>> splashData = [
    {
      "text": "Welcome to DieabeS Library, Let’s read!",
      "image": "assets/images/splash_1.svg"
    },
    {
      "text": "We help people conect with books \naround the world",
      "image": "assets/images/splash_2.svg"
    },
    {
      "text": "We show the easy way to read \nJust hop inside",
      "image": "assets/images/splash_3.svg"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            Expanded(
              flex: 4,
              child: PageView.builder(
                itemCount: splashData.length,
                itemBuilder: (context, index) => SplashContent(
                  text: splashData[index]['text'],
                  image: splashData[index]['image'],
                ),
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: getProportionateScreenWidth(50),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        splashData.length,
                        (index) => buildDot(index: index),
                      ),
                    ),
                    SizedBox(height: getProportionateScreenWidth(70),),
                    currentPage!=splashData.length-1 ?SizedBox():DefaultButton(
                      press: () {
                        Navigator.pushNamed(context, SignIn.routeName);
                      },
                      text: 'Continue',
                    ),
                 
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: Duration(microseconds: 500),
      margin: EdgeInsets.only(right: getProportionateScreenWidth(5)),
      height: getProportionateScreenWidth(6),
      width: getProportionateScreenWidth(6),
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(getProportionateScreenWidth(3)),
      ),
    );
  }
}
