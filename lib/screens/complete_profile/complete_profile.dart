import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';

import 'contents/body.dart';

class CompleteProfile extends StatelessWidget {
  static String routeName = '/complete_profile';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Complete Profile',
          style: TextStyle(
            fontSize: getProportionateScreenWidth(18),
          ),
        ),
        centerTitle: true,
      ),
      body: Body(),
    );
  }
}
