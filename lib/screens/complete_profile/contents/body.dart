import 'package:e_book_store/components/default_button.dart';
import 'package:e_book_store/screens/complete_profile/contents/complete_profile_form.dart';
import 'package:e_book_store/screens/home/home.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SizedBox(
      width: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Text(
              "Complete Your \nInformation",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: getProportionateScreenWidth(28),
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: getProportionateScreenWidth(20),
            ),
            TweenAnimationBuilder(
                tween: Tween<double>(begin: 0, end: 1),
                duration: Duration(milliseconds: 4000),
                curve: Curves.easeIn,
                builder: (context, _, Widget child) {
                  return Opacity(
                    opacity: _,
                    child: SvgPicture.asset(
                      'assets/images/completeProfile.svg',
                      height: getProportionateScreenWidth(200),
                      width: getProportionateScreenWidth(200),
                    ),
                  );
                }),
            CompleteProfileForm(),
            SizedBox(
              height: getProportionateScreenWidth(50),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20)),
              child: DefaultButton(
                  text: 'Continue',
                  press: () {
                    Navigator.pushNamedAndRemoveUntil(context, Home.routeName,
                        ((Route<dynamic> route) => false));
                  }),
            ),
            SizedBox(
              height: getProportionateScreenWidth(50),
            ),
          ],
        ),
      ),
    ));
  }
}
