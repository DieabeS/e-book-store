import 'package:e_book_store/components/custom_suffix_icon.dart';
import 'package:e_book_store/shared/constants.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';

class CompleteProfileForm extends StatefulWidget {
  @override
  _CompleteProfileFormState createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  String value;
  List listItem = ['Male', 'Female'];
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: Column(
          children: [
            SizedBox(
              height: getProportionateScreenWidth(30),
            ),
            Container(
              height: getProportionateScreenWidth(45),

              child: TextFormField(
                decoration: InputDecoration(
                    suffixIcon: CustomSufffixIcon(
                      icon: Icons.person_outline,
                    ),
                    labelText: 'First Name',
                    labelStyle: formTextStyle(),
                    alignLabelWithHint: true),
                style: formTextStyle(),
              ),
            ),
            SizedBox(
              height: getProportionateScreenWidth(30),
            ),
            Container(
              height: getProportionateScreenWidth(45),

              child: TextFormField(
                decoration: InputDecoration(
                    suffixIcon: CustomSufffixIcon(
                      icon: Icons.person_outline,
                    ),
                    labelText: 'Last Name',
                    labelStyle: formTextStyle(),
                    alignLabelWithHint: true),
                style: formTextStyle(),
              ),
            ),
            SizedBox(
              height: getProportionateScreenWidth(30),
            ),
            Container(
              height: getProportionateScreenWidth(45),

              child: TextFormField(
                decoration: InputDecoration(
                    suffixIcon: CustomSufffixIcon(
                      icon: Icons.date_range_outlined,
                    ),
                    labelText: 'Date of Birth',
                    labelStyle: formTextStyle(),
                    alignLabelWithHint: true),
                style: formTextStyle(),
                readOnly: true,
              ),
            ),
            SizedBox(
              height: getProportionateScreenWidth(30),
            ),
            Container(
              height: getProportionateScreenWidth(43),
              alignment: Alignment.center,
              padding: EdgeInsets.only(
                  left: getProportionateScreenWidth(30),
                  right: getProportionateScreenWidth(10)),
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                    width: getProportionateScreenWidth(2),
                  ),
                  borderRadius:
                      BorderRadius.circular(getProportionateScreenWidth(28))),
              child: DropdownButton(
                style: TextStyle(
                    fontSize: getProportionateScreenWidth(17),
                    color: Colors.black),
                underline: SizedBox(),
                hint: Text(
                  'Gender',
                  style: TextStyle(fontSize: getProportionateScreenWidth(17)),
                ),
                iconSize: getProportionateScreenWidth(20),
                isExpanded: true,
                items: listItem.map((valueItem) {
                  return DropdownMenuItem(
                      child: Text(valueItem), value: valueItem);
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    value = newValue;
                  });
                },
                value: value,
              ),
            )
          ],
        ),
      ),
    );
  }
}
