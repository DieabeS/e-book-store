import 'package:e_book_store/components/default_button.dart';
import 'package:e_book_store/screens/sign_in/contents/sign_in_form.dart';
import 'package:e_book_store/screens/sign_up/sign_up.dart';
import 'package:e_book_store/shared/constants.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
       
              Text(
                "Welcome Back",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: getProportionateScreenWidth(28),
                  fontWeight: FontWeight.bold,
                ),
              ),
                     SizedBox(
                height: getProportionateScreenWidth(20),
              ),
              TweenAnimationBuilder(
                  tween: Tween<double>(begin: 0, end: 1),
                  duration: Duration(milliseconds: 4000),
                  curve: Curves.easeIn,
                  builder: (context, _, Widget child) {
                    return Opacity(
                      opacity: _,
                      child: SvgPicture.asset(
                        'assets/images/signIn.svg',
                        height: getProportionateScreenWidth(200),
                        width: getProportionateScreenWidth(200),
                      ),
                    );
                  }),
              SizedBox(
                height: getProportionateScreenWidth(30),
              ),
              SignInForm(),
              SizedBox(
                height: getProportionateScreenWidth(50),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: DefaultButton(text: 'Sign In', press: () {}),
              ),
              SizedBox(
                height: getProportionateScreenWidth(30),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Don’t have an account? ",
                    style: TextStyle(fontSize: getProportionateScreenWidth(16)),
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.pushNamed(context, SignUp.routeName);
                    },
                    child: Text(
                      "Sign Up",
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(16),
                          color: kPrimaryColor),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
