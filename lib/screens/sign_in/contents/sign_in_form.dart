import 'package:e_book_store/components/custom_suffix_icon.dart';
import 'package:e_book_store/shared/constants.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';

class SignInForm extends StatefulWidget {
  @override
  _SignInFormState createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: Column(
          children: [
            buildEmailForm(),
            SizedBox(
              height: getProportionateScreenWidth(30),
            ),
            buildPasswordForm(),
            SizedBox(
              height: getProportionateScreenWidth(40),
            ),
            GestureDetector(
              onTap: () {},
              child: Text(
                "Forgot Password?",
                style: TextStyle(
                    decoration: TextDecoration.underline,
                    fontSize: getProportionateScreenWidth(16)),
              ),
            )
          ],
        ),
      ),
    );
  }

  TextFormField buildPasswordForm() {
    return TextFormField(
      decoration: InputDecoration(
          suffixIcon: CustomSufffixIcon(
            icon: Icons.lock_outline,
          ),
          labelText: 'Password',
          labelStyle: formTextStyle(),
          alignLabelWithHint: true),
      obscureText: true,
      style: formTextStyle(),
    );
  }

  TextFormField buildEmailForm() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          suffixIcon: CustomSufffixIcon(
            icon: Icons.mail_outline,
          ),
          labelText: 'Email',
          labelStyle: formTextStyle(),
          alignLabelWithHint: true),
      style: formTextStyle(),
    );
  }
}
