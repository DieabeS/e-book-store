import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';

import 'contents/body.dart';

class SignIn extends StatelessWidget {
  static String routeName = '/sign_in';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Sign In',
          style: TextStyle(
            fontSize: getProportionateScreenWidth(18),
          ),
        ),
        centerTitle: true,
      ),
      body: Body(),
    );
  }
}
