import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  static String routeName = '/home';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'DieabeS Library',
          style: TextStyle(
              fontSize: getProportionateScreenWidth(25),
              fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
        leading: Icon(
          Icons.menu,
          size: getProportionateScreenWidth(30),
          color: Colors.black54,
        ),
        leadingWidth: getProportionateScreenWidth(56),
        actions: [
          Icon(
            Icons.search,
            size: getProportionateScreenWidth(30),
            color: Colors.black54,
          )
        ],
      ),
      body: Container(
        child: SingleChildScrollView(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(15)),
          child: Column(
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Popular Books',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: getProportionateScreenWidth(20)),
                  ),
                  Text(
                    'view all',
                    style: TextStyle(
                        fontSize: getProportionateScreenWidth(15),
                        color: Colors.black54),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
