import 'package:device_preview/device_preview.dart';
import 'package:e_book_store/screens/splash/splash.dart';
import 'package:e_book_store/shared/routes.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:e_book_store/shared/theme.dart';
import 'package:flutter/material.dart';

void main() {

 

  runApp((MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              builder: DevicePreview.appBuilder,
              title: 'E-Book Store',
              debugShowCheckedModeBanner: false,
              theme: theme(),
              home: Splash(),
              routes: routes,
            );
          },
        );
      },
    );
  }
}
