import 'package:flutter/material.dart';

import '../shared/size_config.dart';

class CustomSufffixIcon extends StatelessWidget {
  const CustomSufffixIcon({
    Key key,
    @required this.icon,
  }) : super(key: key);

  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
        getProportionateScreenWidth(8),
        getProportionateScreenWidth(12),
        getProportionateScreenWidth(8),
        getProportionateScreenWidth(12),
      ),
      child: Icon(
        icon,
        size: getProportionateScreenWidth(20),
      ),
    );
  }
}
