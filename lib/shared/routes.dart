
import 'package:e_book_store/screens/complete_profile/complete_profile.dart';
import 'package:e_book_store/screens/home/home.dart';
import 'package:e_book_store/screens/sign_in/sign_in.dart';
import 'package:e_book_store/screens/sign_up/sign_up.dart';
import 'package:e_book_store/screens/splash/splash.dart';
import 'package:flutter/material.dart';

final Map<String, WidgetBuilder> routes = {

Splash.routeName:(_)=>Splash(),
SignIn.routeName:(_)=>SignIn(),
SignUp.routeName:(_)=>SignUp(),
CompleteProfile.routeName:(_)=>CompleteProfile(),
Home.routeName:(_)=> Home(),


  

};
