
import 'package:e_book_store/shared/constants.dart';
import 'package:e_book_store/shared/size_config.dart';
import 'package:flutter/material.dart';



ThemeData theme() {
  return ThemeData(
    scaffoldBackgroundColor: Colors.white,
    fontFamily: "Muli",
    appBarTheme: appBarTheme(),
    inputDecorationTheme: inputDecorationTheme(),
    visualDensity: VisualDensity.adaptivePlatformDensity,primaryColor: kPrimaryColor
  );
}

InputDecorationTheme inputDecorationTheme() {
  OutlineInputBorder outlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(28)),
    borderSide: BorderSide(color: Colors.grey,width: getProportionateScreenWidth(2)),
    gapPadding: getProportionateScreenWidth(10),
  );
  return InputDecorationTheme(
    floatingLabelBehavior: FloatingLabelBehavior.always,
    contentPadding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(30),),
    enabledBorder: outlineInputBorder,
    focusedBorder: outlineInputBorder,fillColor: Colors.white,filled: true,
    border: outlineInputBorder,
  );
}



AppBarTheme appBarTheme() {
  return AppBarTheme(
    color: Colors.white,
    elevation: 0,
    brightness: Brightness.light,
    iconTheme: IconThemeData(color: Colors.black),
    textTheme: TextTheme(
      headline6: TextStyle(color: Color(0XFF8B8B8B), fontSize: getProportionateScreenWidth(18)),
    ),
  );
}