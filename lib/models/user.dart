class User {
  User(
      {this.firstName = '',
      this.lastName = '',
      this.email = '',
      this.password = '',
      this.birthdayDate='',
      this.gender='',
      this.isNotificationEnabled = true,
      this.balance = 0});
  String firstName;
  String lastName;
  String email;
  String password;
  String birthdayDate;
  String gender;
  bool isNotificationEnabled;
  double balance;
}
