class Book {
  Book(
      {this.ref = '',
      this.coverImage = '',
      this.title = '',
      this.description = '',
      this.language = '',
      this.iSBN13 = '',
      this.pageCount = 0,
      this.listPrice = 0,
      this.averageRating = 0.0,});
  String ref;
  String coverImage;
  String title;
  String description;
  String language;
  String iSBN13;
  int pageCount;
  int listPrice;
  double averageRating;
}
